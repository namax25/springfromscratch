package com.maxwww;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        System.out.println("Start");
        try {

            BeanFactory beanFactory = new BeanFactory();
            beanFactory.addPostProcessor(new CustomPostProcessor());
            beanFactory.instantiate("com.maxwww");
            beanFactory.populateProperties();
            beanFactory.injectBeanNames();
            beanFactory.initializeBeans();

            ProductService productService = (ProductService) beanFactory.getBean("productService");
            System.out.println(productService);


            PromotionsService promotionsService = productService.getPromotionsService();

            System.out.println("Bean name = " + promotionsService.getBeanName());

            ApplicationContext applicationContext = new ApplicationContext("com.maxwww");
            applicationContext.close();

            beanFactory.close();
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }
}
